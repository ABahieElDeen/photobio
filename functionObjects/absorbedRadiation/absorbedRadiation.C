/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2016-2017 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "absorbedRadiation.H"
#include "surfaceInterpolate.H"
#include "fvcSnGrad.H"
#include "wallPolyPatch.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace functionObjects
{
    defineTypeNameAndDebug(absorbedRadiation, 0);
    addToRunTimeSelectionTable(functionObject, absorbedRadiation, dictionary);
}
}


// * * * * * * * * * * * * * Protected Member Functions  * * * * * * * * * * //

void Foam::functionObjects::absorbedRadiation::writeFileHeader(const label i)
{
    // Add headers to output data
    writeHeader(file(), "Absorbed radiation");
    writeCommented(file(), "Time");
    writeTabbed(file(), "band");
    writeTabbed(file(), "integral");
    file() << endl;
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::functionObjects::absorbedRadiation::absorbedRadiation
(
    const word& name,
    const Time& runTime,
    const dictionary& dict
)
:
    fvMeshFunctionObject(name, runTime, dict),
    logFiles(obr_, name),
    nBands_(readLabel(dict.lookup("nBands")))
{
    // Set size of absorbed radiation list to number of wavelength bands
    absorbedRadiation_.setSize(nBands_);

    read(dict);
    resetName(typeName);
}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::functionObjects::absorbedRadiation::~absorbedRadiation()
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

bool Foam::functionObjects::absorbedRadiation::read(const dictionary& dict)
{
    fvMeshFunctionObject::read(dict);

    Info << type() << " " << name() << nl;

    return true;
}


bool Foam::functionObjects::absorbedRadiation::execute()
{
    // Calculate the absorbed radiation for each wavelength band
    for (label iBand = 0; iBand < nBands_; iBand++)
    {
        // Get the spectral absorptivity and irradiation fields
        word AName = "ALambda_" + Foam::name(iBand);
        const scalarField& A = lookupObject<scalarField>(AName);
        word GName = "GLambda_" + Foam::name(iBand);
        const scalarField& G = lookupObject<scalarField>(GName);

        // Get the cell volumes
        const scalarField& V = mesh_.V();

        // Calculate the absorbed radiation
        absorbedRadiation_[iBand] = gSum(A*G*V);
    }

    return true;
}


bool Foam::functionObjects::absorbedRadiation::write()
{
    Log << type() << " " << name() << " write:" << nl;

    logFiles::write();

    for (label iBand = 0; iBand < nBands_; iBand++)
    {
        if (Pstream::master())
        {
            file()
                << mesh_.time().value()
                << token::TAB << iBand
                << token::TAB << absorbedRadiation_[iBand]
                << endl;
        }

        Log << "    integ(absorbedRadiation, iBand = " << iBand
            << ") = " << absorbedRadiation_[iBand] << endl;
    }

    Log << endl;

    return true;
}


// ************************************************************************* //
